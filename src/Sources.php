<?php


namespace Kito\Loader;

class Sources
{
	public static function attach(\Kito\Loader $loader)
	{
		$loader->addRepository('/Psr/Log',    'https://raw.githubusercontent.com/php-fig/log/master/Psr/Log');
		$loader->addRepository('/Psr/Http/Message', 'https://raw.githubusercontent.com/php-fig/http-message/master/src');
		$loader->addRepository('/Psr/Http/Message', 'https://raw.githubusercontent.com/php-fig/http-factory/master/src');
		$loader->addRepository('/Psr/Http/Server', 'https://raw.githubusercontent.com/php-fig/http-server-handler/master/src');                
		$loader->addRepository('/Psr/Http/Server', 'https://raw.githubusercontent.com/php-fig/http-server-middleware/master/src/');                


		$loader->addRepository('/Fig/Http/Message', 'https://raw.githubusercontent.com/php-fig/http-message-util/master/src/');                


		$loader->addRepository('/FastRoute',    'https://raw.githubusercontent.com/nikic/FastRoute/master/src/');
		$loader->addRepository('/FastRoute',    'https://raw.githubusercontent.com/TheKito/FastRoute/master/src/');


		$loader->addRepository('/Slim',    'https://raw.githubusercontent.com/TheKito/Slim/4.x/Slim/');
		$loader->addRepository('/Slim/Psr7',    'https://raw.githubusercontent.com/slimphp/Slim-Psr7/master/src/');
		$loader->addRepository('/Slim/Http',    'https://raw.githubusercontent.com/slimphp/Slim-Http/master/src/');


		$loader->addRepository('/Kito',    'https://raw.githubusercontent.com/TheKito/Proxy-Memcache/master/src/Kito');
		$loader->addRepository('/Kito/DataBase/SQL',    'https://raw.githubusercontent.com/TheKito/Proxy-SQL/master/src/Kito/DataBase/SQL');

		$loader->addRepository('/Kito/DataBase/NoSQL',    'https://raw.githubusercontent.com/TheKito/DataBase-NoSQL-KeyValue/master/src/Kito/DataBase/NoSQL');
		

		$loader->addRepository('/Guzzle/Http', 'https://raw.githubusercontent.com/thekito/guzzle/master/src');

		$loader->addRepository('/Symfony',    'https://raw.githubusercontent.com/symfony/symfony/master/src/Symfony');

		$loader->addRepository('/Illuminate', 'https://raw.githubusercontent.com/laravel/framework/6.x/src');


		$loader->addRepository('/Datto/JsonRpc',    	'https://github.com/datto/php-json-rpc/blob/master/src');
		$loader->addRepository('/Datto/JsonRpc/Http',   'https://github.com/datto/php-json-rpc-http/blob/master/src');
		$loader->addRepository('/Datto/JsonRpc/Ssh',    'https://github.com/datto/php-json-rpc-ssh/blob/master/src');

		$loader->addRepository('/Gnello/OpenFireRestAPI',    'https://raw.githubusercontent.com/gnello/php-openfire-restapi/master/src');
                
		$loader->addRepository('/Kito/Proxy',    'https://raw.githubusercontent.com/TheKito/proxy/master/src');
                

	}
}
